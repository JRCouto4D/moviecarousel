import React, { useCallback, createContext, useContext } from 'react';
import { useDispatch } from 'react-redux';

import { addToast, showAlert } from '../store/ducks/notifications/actions';
import { Message, Alert } from '../store/ducks/notifications/types';

interface NotificationProps {
  alert(content: Alert): void;
  toast(message: Message): void;
}

const NotificationContext = createContext<NotificationProps>({
  alert: () => {
    // console.log('');
  },
  toast: () => {
    // console.log('');
  },
});

const NotificationProvider: React.FC = ({ children }) => {
  const dispatch = useDispatch();

  const toast = useCallback(
    (message: Message) => {
      dispatch(addToast(message));
    },
    [dispatch],
  );

  const alert = useCallback(
    (content: Alert) => {
      dispatch(showAlert(content));
    },
    [dispatch],
  );

  return (
    <NotificationContext.Provider value={{ toast, alert }}>
      {children}
    </NotificationContext.Provider>
  );
};

function useNotification() {
  const context = useContext(NotificationContext);

  if (!context) {
    throw new Error(
      'useNotification deve ser usado em um NotificationProvider',
    );
  }

  return context;
}

export { NotificationProvider, useNotification };
