import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';

const ww = (value: number) => responsiveWidth(value);
const wh = (value: number) => responsiveHeight(value);

const WIDTH = ww(100);
const HEIGHT = wh(100);

export { ww, wh, WIDTH, HEIGHT };
