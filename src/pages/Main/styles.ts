import { StyleSheet } from 'react-native';
import { ww } from '../../utils/dimensions';

const SPACING = 10;
const ITEM_WIDTH = ww(70);
const ITEM_HEIGHT = ITEM_WIDTH * 1.6;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#312e38',
  },
  content: {
    width: ww(100),
    justifyContent: 'center',
    padding: SPACING * 2,
  },
  itemContainer: {
    position: 'absolute',
    left: -ITEM_WIDTH / 2,
  },
  poster: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    borderRadius: 20,
  },
});

export default styles;
