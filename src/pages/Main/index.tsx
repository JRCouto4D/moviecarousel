import React, { useCallback, useEffect, useRef, useState } from 'react';
import { SafeAreaView, Image, ScrollView, Animated, View } from 'react-native';
import {
  Directions,
  FlingGestureHandler,
  State,
} from 'react-native-gesture-handler';

import movies from '../../services/movies';

import Rating from '../../components/Rating';
import OverflowView from '../../components/OverflowView';
import Genres from '../../components/Genres';
import Skip from '../../components/Skip';
import Background from '../../components/Background';
import styles from './styles';

// import { Container, Text } from './styles';

const VISIBLE_ITENS = 3;

const Main: React.FC = () => {
  const scrollXIndex = useRef(new Animated.Value(0)).current;
  const scrollXAnimated = useRef(new Animated.Value(0)).current;

  const [indeX, setIndex] = useState(0);

  useEffect(() => {
    Animated.spring(scrollXAnimated, {
      toValue: scrollXIndex,
      useNativeDriver: true,
    }).start();

    // setInterval(() => {
    //   scrollXIndex.setValue(Math.floor(Math.random() * movies.length));
    // }, 3000);
  });

  const setActiveIndex = useCallback(
    (activeIndex: number) => {
      scrollXIndex.setValue(activeIndex);
      setIndex(activeIndex);
    },
    [scrollXIndex],
  );

  return (
    <FlingGestureHandler
      key="left"
      direction={Directions.LEFT}
      onHandlerStateChange={evt => {
        if (evt.nativeEvent.state === State.END) {
          if (indeX === movies.length - 1) return;

          setActiveIndex(indeX + 1);
        }
      }}>
      <FlingGestureHandler
        key="right"
        direction={Directions.RIGHT}
        onHandlerStateChange={evt => {
          if (evt.nativeEvent.state === State.END) {
            if (indeX === 0) return;

            setActiveIndex(indeX - 1);
          }
        }}>
        <SafeAreaView style={styles.container}>
          <Background current={indeX} />

          <OverflowView data={movies} scrollXAnimated={scrollXAnimated} />

          <ScrollView
            style={{ marginTop: '20%' }}
            contentContainerStyle={styles.content}
            scrollEnabled={false}
            horizontal
            removeClippedSubviews={false}>
            <View>
              {movies.map((item, index) => {
                const inputRange = [index - 1, index, index + 1];
                const translateX = scrollXAnimated.interpolate({
                  inputRange,
                  outputRange: [50, 0, -100],
                });

                const scale = scrollXAnimated.interpolate({
                  inputRange,
                  outputRange: [0.8, 1, 1.3],
                });

                const opacity = scrollXAnimated.interpolate({
                  inputRange,
                  outputRange: [1 - 1 / VISIBLE_ITENS, 1, 0],
                });

                return (
                  <Animated.View
                    key={item.key}
                    style={[
                      styles.itemContainer,
                      {
                        opacity,
                        zIndex: movies.length - index,
                        transform: [
                          {
                            translateX,
                          },
                          {
                            scale,
                          },
                        ],
                      },
                    ]}>
                    <Image
                      source={{ uri: item.poster }}
                      style={styles.poster}
                    />

                    <Rating rating={item.rating} />
                    <Genres data={item.genres} />
                  </Animated.View>
                );
              })}
            </View>
          </ScrollView>
          <Skip current={indeX} />
        </SafeAreaView>
      </FlingGestureHandler>
    </FlingGestureHandler>
  );
};

export default Main;
