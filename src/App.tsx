import { GestureHandlerRootView } from 'react-native-gesture-handler';
import React from 'react';
import { LogBox, StatusBar } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';

import { persistor, store } from './store';

import AppProvider from './hooks';
import Routes from './routes/index.routes';

import Toast from './components/Notifications/Toast';
import Alert from './components/Notifications/Alert';

const App: React.FC = () => {
  LogBox.ignoreLogs(['Expected style']);

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <AppProvider>
          <GestureHandlerRootView style={{ flex: 1 }}>
            <StatusBar
              barStyle="light-content"
              translucent
              backgroundColor="#000"
            />
            <Routes />

            <Toast />
            <Alert />
          </GestureHandlerRootView>
        </AppProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
