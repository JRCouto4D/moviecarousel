import { Reducer } from 'redux';
import uuid from 'react-native-uuid';

import { NotificationsState, NotificationsTypes } from './types';

const INITIAL_STATE: NotificationsState = {
  toast: {
    messages: [],
  },
  alerts: {
    alert: null,
  },
};

const reducer: Reducer<NotificationsState> = (
  state = INITIAL_STATE,
  action,
) => {
  switch (action.type) {
    case NotificationsTypes.ADD_TOAST: {
      const { message } = action.payload;

      const bodyMessage = {
        id: `${uuid.v4()}`,
        ...message,
      };

      return {
        ...state,
        toast: { messages: [...state.toast.messages, bodyMessage] },
      };
    }

    case NotificationsTypes.REMOVE_TOAST: {
      const { id } = action.payload;

      const currentMessages = state.toast.messages.filter(
        message => message.id !== id,
      );

      return { ...state, toast: { messages: currentMessages } };
    }

    case NotificationsTypes.SHOW_ALERT: {
      const { alert } = action.payload;

      return {
        ...state,
        alerts: {
          alert,
        },
      };
    }

    case NotificationsTypes.HIDE_ALERT: {
      return {
        ...state,
        alerts: {
          alert: null,
        },
      };
    }

    default:
      return state;
  }
};

export default reducer;
