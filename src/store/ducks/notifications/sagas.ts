import { all, takeLatest } from 'redux-saga/effects';
import { NotificationsTypes } from './types';

export function actionAddToast() {
  // console.log('true');
}

export default all([takeLatest(NotificationsTypes.ADD_TOAST, actionAddToast)]);
