/**
 * ACTIONS TYPES
 */
export enum NotificationsTypes {
  ADD_TOAST = '@notifications/ADD_TOAST',
  REMOVE_TOAST = '@notifications/REMOVE_TOAST',

  SET_ALERT = '@notifications/SET_ALERT',
  SHOW_ALERT = '@notifications/SHOW_ALERT',
  HIDE_ALERT = '@notifications/HIDE_ALERT',
}

/**
 * DATA TYPES
 */

export interface Message {
  id: string;
  type: 'error' | 'info' | 'success';
  title: string;
  description: string;
}

interface Toast {
  messages: Message[];
}

export interface Button {
  text: string;
  onPress?(): void;
  style?: 'default' | 'cancel';
}

export interface Alert {
  title: string;
  description?: string;
  buttons?: Button[];
}

interface Alerts {
  alert: Alert | null;
}

/**
 * STATE TYPE
 */

export interface NotificationsState {
  readonly toast: Toast;
  readonly alerts: Alerts;
}
