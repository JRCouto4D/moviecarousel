import { action } from 'typesafe-actions';
import { Alert, NotificationsTypes } from './types';

interface MessageTypes {
  type: 'error' | 'info' | 'success';
  title: string;
  description: string;
}

export const addToast = (message: MessageTypes) =>
  action(NotificationsTypes.ADD_TOAST, { message });

export const removeToast = (id: string) =>
  action(NotificationsTypes.REMOVE_TOAST, { id });

export const showAlert = (alert: Alert) =>
  action(NotificationsTypes.SHOW_ALERT, { alert });

export const hideAlert = () => action(NotificationsTypes.HIDE_ALERT);
