import { all } from 'redux-saga/effects';

import user from './user/sagas';
import notifications from './notifications/sagas';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default function* rootSaga(): any {
  return yield all([user, notifications]);
}
