import { Reducer } from 'redux';
import { UserState, UserTypes } from './types';

const INITIAL_STATE: UserState = {
  user: null,
  loading: false,
  error: false,
};

const reducer: Reducer<UserState> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    /**
     * HANDLE USER PROFILE
     */

    case UserTypes.GET_PROFILE_REQUEST: {
      return { ...state, loading: true };
    }

    case UserTypes.GET_PROFILE_SUCCESS: {
      const { user } = action.payload;

      return { ...state, user, loading: false, error: false };
    }

    case UserTypes.GET_PROFILE_FAILURE: {
      return { ...state, user: null, loading: false, error: true };
    }

    default:
      return state;
  }
};

export default reducer;
