import { action } from 'typesafe-actions';
import { UserTypes, User } from './types';

/**
 * HANDLE USER PROFILE
 */

export interface ProfileRequestPayloadTypes {
  nickName: string;
}

export const getProfileRequest = (nickName: string) =>
  action(UserTypes.GET_PROFILE_REQUEST, { nickName });

export const getProfileSuccess = (user: User) =>
  action(UserTypes.GET_PROFILE_SUCCESS, { user });

export const getProfileFailure = () => action(UserTypes.GET_PROFILE_FAILURE);
