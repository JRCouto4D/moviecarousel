import { all, call, put, takeLatest } from 'redux-saga/effects';
import { Action } from 'redux';
import { Alert } from 'react-native';

import api from '../../../services/api';

import { UserTypes } from './types';
import {
  ProfileRequestPayloadTypes,
  getProfileSuccess,
  getProfileFailure,
} from './actions';

interface payloadProp extends Action {
  payload: ProfileRequestPayloadTypes;
}

export function* getUserProfile({ payload }: payloadProp): unknown {
  const { nickName } = payload;

  try {
    const response = yield call(api.get, `users/${nickName}/profile`);

    yield put(getProfileSuccess(response.data));
  } catch (error) {
    Alert.alert('Something wrong', 'Unable to load user profile');
    yield put(getProfileFailure());
  }
}

export default all([takeLatest(UserTypes.GET_PROFILE_REQUEST, getUserProfile)]);
