/**
 * Action types
 */

export enum UserTypes {
  GET_PROFILE_REQUEST = '@user/GET_PROFILE_REQUEST',
  GET_PROFILE_SUCCESS = '@user/GET_PROFILE_SUCCESS',
  GET_PROFILE_FAILURE = '@user/GET_PROFILE_FAILURE',
}

export interface User {
  id: number;
  name: string;
  avatar: string;
  bio: string;
}

/**
 * State type
 */

export interface UserState {
  readonly user: User | null;
  readonly loading: boolean;
  readonly error: boolean;
}
