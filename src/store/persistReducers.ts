import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer } from 'redux-persist';
import { Reducer } from 'redux';

export default (reducers: Reducer) => {
  const persistedReducer = persistReducer(
    {
      key: 'root',
      storage: AsyncStorage,
      whitelist: ['user'],
    },
    reducers,
  );

  return persistedReducer;
};
