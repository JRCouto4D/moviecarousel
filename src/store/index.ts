import { persistStore } from 'redux-persist';
import { createStore, Store, applyMiddleware } from 'redux';
import createSageMiddleware from 'redux-saga';

import { UserState } from './ducks/user/types';
import { NotificationsState } from './ducks/notifications/types';

import persistReducers from './persistReducers';

import rootReducer from './ducks/rootReducer';
import rootSaga from './ducks/rootSaga';

export interface ApplicationState {
  user: UserState;
  notifications: NotificationsState;
}

// const sagaMonitor = __DEV__ ? console.tron.createSagaMonitor() : null;
const sagaMonitor = null || undefined;

const sagaMiddleware = createSageMiddleware({ sagaMonitor });

const middlewares = [sagaMiddleware];

// const enhancer = __DEV__
//   ? compose(console.tron.createEnhancer(), applyMiddleware(...middleware))
//   : applyMiddleware(...middleware);
const enhancer = applyMiddleware(...middlewares);

const store: Store<ApplicationState> = createStore(
  persistReducers(rootReducer),
  enhancer,
);

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };
