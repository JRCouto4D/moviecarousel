export interface MoviesType {
  key: string;
  title: string;
  genres: Array<string>;
  debut: string;
  rating: number;
  where: string;
  poster: string;
}

const movies: MoviesType[] = [
  {
    key: '1',
    title: 'WandaVision',
    genres: ['Ação', 'Comédia', 'Fantasia'],
    debut: '2021-01-15T00:00:00-03:00',
    rating: 8.5,
    where: 'Disney+',
    poster:
      'https://m.media-amazon.com/images/M/MV5BZGEwYmMwZmMtMTQ3MS00YWNhLWEwMmQtZTU5YTIwZmJjZGQ0XkEyXkFqcGdeQXVyMTI5MzA5MjA1._V1_FMjpg_UX1000_.jpg',
  },
  {
    key: '2',
    title: 'Falcão e o Soldado Invernal',
    genres: ['Ação', 'Aventura', 'Fantasia'],
    debut: '2021-03-19T00:00:00-03:00',
    rating: 7.9,
    where: 'Disney+',
    poster:
      'https://img.elo7.com.br/product/original/3816C9D/big-poster-falcao-e-soldado-invernal-lo001-decoracao-geek.jpg',
  },
  {
    key: '3',
    title: 'Loki',
    genres: ['Fantasia', 'Ficção Cientifica'],
    debut: '2021-06-09T00:00:00-03:00',
    rating: 9.8,
    where: 'Disney+',
    poster:
      'https://cdn.ome.lt/dmwIdggslj4YfYCBooJrZ-F-mqY=/fit-in/1070x750/smart/filer_public/99/39/9939cdd8-be20-4a3b-b13a-79b44715869a/variante-4.png',
  },
  {
    key: '4',
    title: 'Lupin - Part 2',
    genres: ['Policial', 'Drama'],
    debut: '2021-06-09T00:00:00-03:00',
    rating: 7.5,
    where: 'Netflix',
    poster:
      'https://img.elo7.com.br/product/zoom/3CA4491/poster-lupin-poster-de-serie.jpg',
  },
  {
    key: '5',
    title: 'The Witcher - Season 2',
    genres: ['Fantasia', 'Aventura'],
    debut: '2021-12-16T00:00:00-03:00',
    rating: 9.5,
    where: 'Netflix',
    poster:
      'https://cdn.shopify.com/s/files/1/2356/1293/products/TheWitcher-Season2BlizzardPosterEgoamo.co.zaPosters_800x.jpg?v=1623085535',
  },
  {
    key: '6',
    title: 'Superman & Lois - Season 2',
    genres: ['Ação', 'Aventura', 'Drama', 'Fantasia'],
    debut: '2022-01-11T00:00:00-03:00',
    rating: 6.4,
    where: 'HBO Max',
    poster:
      'https://oyster.ignimgs.com/wordpress/stg.ign.com/2021/01/SML_S1_8x12_300dpi.jpg?fit=bounds&width=640&height=480',
  },
];

export default movies;
