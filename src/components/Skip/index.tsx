import React from 'react';
import { View } from 'react-native';
import movies from '../../services/movies';

import styles from './styles';

interface SkipProps {
  current: number;
}

const Skip: React.FC<SkipProps> = ({ current = 0 }) => {
  return (
    <View style={styles.container}>
      {movies.map((_, index) => (
        <View
          key={String(index)}
          style={[
            { backgroundColor: current === index ? 'tomato' : '#666' },
            styles.skip,
          ]}
        />
      ))}
    </View>
  );
};

export default Skip;
