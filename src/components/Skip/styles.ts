import { StyleSheet } from 'react-native';
import { WIDTH, wh } from '../../utils/dimensions';

const RATIO_SKIP = 10;

const styles = StyleSheet.create({
  container: {
    width: WIDTH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: wh(10),
  },
  skip: {
    width: RATIO_SKIP,
    height: RATIO_SKIP,
    borderRadius: RATIO_SKIP / 2,
    marginHorizontal: 2,
  },
});

export default styles;
