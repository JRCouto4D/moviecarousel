import React, {
  useState,
  useRef,
  useEffect,
  useImperativeHandle,
  forwardRef,
  useCallback,
} from 'react';
import { TextInputProps } from 'react-native';
import { useField } from '@unform/core';

import { Container, FeatherIcon, MaterialIcon, TextInput } from './styles';

interface IconProps {
  type: 'feather' | 'material';
  name: string;
  color?: string;
  colorInFocus?: string;
}

interface InputProps extends TextInputProps {
  name: string;
  icon?: IconProps;
  containerStyle?: Record<string, unknown>;
  inputStyle?: Record<string, unknown>;
  checkError?(value: string, name: string): boolean;
}

interface InputValureReference {
  value: string;
}

interface inputRef {
  focus(): void;
}

const Input: React.RefForwardingComponent<inputRef, InputProps> = (
  { name, icon, containerStyle, inputStyle, checkError, ...rest },
  ref,
) => {
  const {
    registerField,
    defaultValue = '',
    error,
    fieldName,
    clearError,
  } = useField(name);

  const inputValueRef = useRef<InputValureReference>({ value: defaultValue });
  const inputElementRef = useRef<any>(null);

  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const handleInputFucos = useCallback(() => setIsFocused(true), []);

  const handleInputBlur = useCallback(() => {
    if (checkError) {
      const checkErrored = checkError(inputValueRef.current?.value, name);

      if (checkErrored) {
        clearError();
      }
    }

    setIsFocused(false);
    setIsFilled(!!inputValueRef.current.value);
  }, [checkError, inputValueRef, name, clearError]);

  useImperativeHandle(ref, () => ({
    focus() {
      inputElementRef.current.focus();
    },
  }));

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputValueRef.current,
      path: 'value',
      setValue(ref: any, value: string) {
        inputValueRef.current.value = value;
        inputElementRef.current?.setNativeProps({ text: value });
      },
      clearValue() {
        inputValueRef.current.value = '';
        inputElementRef.current?.clear();
      },
    });
  }, [fieldName, registerField]);

  return (
    <Container
      isFocused={isFocused}
      isErrored={!!error}
      colorInFocus={icon?.colorInFocus || '#666360'}
      style={containerStyle}>
      {icon &&
        (icon.type === 'feather' ? (
          <FeatherIcon
            name={icon?.name || 'mail'}
            size={20}
            color={
              error
                ? '#c53030'
                : isFocused || isFilled
                ? icon?.colorInFocus || '#666360'
                : icon?.color || '#666360'
            }
          />
        ) : (
          <MaterialIcon
            name={icon?.name || 'email'}
            size={20}
            color={
              error
                ? '#c53030'
                : isFocused || isFilled
                ? icon?.colorInFocus || '#666360'
                : icon?.color || '#666360'
            }
          />
        ))}
      <TextInput
        ref={inputElementRef}
        defaultValue={defaultValue}
        onChangeText={value => {
          inputValueRef.current.value = value;
        }}
        onFocus={handleInputFucos}
        onBlur={handleInputBlur}
        style={inputStyle}
        {...rest}
      />
    </Container>
  );
};

export default forwardRef(Input);
