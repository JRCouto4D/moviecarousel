import styled, { css } from 'styled-components/native';
import {
  responsiveHeight as wh,
  responsiveFontSize as ftsize,
} from 'react-native-responsive-dimensions';
import IconF from 'react-native-vector-icons/Feather';
import IconM from 'react-native-vector-icons/MaterialIcons';

interface InputProps {
  isFocused: boolean;
  isErrored: boolean;
  colorInFocus: string;
}

export const Container = styled.View<InputProps>`
  width: 100%;
  height: ${wh(6)};

  background: #232129;

  border-color: #232129;
  border-width: 1.5px;
  border-radius: 10px;
  margin-bottom: 2.5%;

  flex-direction: row;
  align-items: center;

  padding: 0 4%;

  ${props =>
    props.isFocused &&
    css`
      border-color: ${props.colorInFocus || '#232129'};
    `}

  ${props =>
    props.isErrored &&
    css`
      border-color: #c53030;
    `}
`;

export const FeatherIcon = styled(IconF)``;

export const MaterialIcon = styled(IconM)``;

export const TextInput = styled.TextInput`
  flex: 1;
  color: #fff;
  font-size: ${ftsize(1.6)};

  margin-left: 2%;
`;
