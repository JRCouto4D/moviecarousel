import React from 'react';
import { Text, View } from 'react-native';
import Material from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';

interface RatingProps {
  rating: number;
}

const Rating: React.FC<RatingProps> = ({ rating }) => {
  const filledStars = Math.floor(rating / 2);
  const maxStars = Array(5 - filledStars).fill({
    name: 'star-border',
    color: '#999',
  });
  const current = [
    ...Array(filledStars).fill({ name: 'star', color: 'tomato' }),
    ...maxStars,
  ];

  return (
    <View style={styles.container}>
      <Text style={styles.rating}>{rating}</Text>

      {current.map((item, index) => (
        <Material
          key={String(index)}
          name={item.name}
          size={12}
          color={item.color}
        />
      ))}
    </View>
  );
};

export default Rating;
