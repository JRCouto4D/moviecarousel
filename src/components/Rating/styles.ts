import { StyleSheet } from 'react-native';

const SPACING = 10;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: SPACING,
  },
  rating: {
    fontFamily: 'Menlo',
    fontSize: 12,
    marginRight: SPACING / 2,
  },
});

export default styles;
