import React from 'react';
import { TouchableOpacityProps } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { Container } from './styles';

interface ButtonProps extends TouchableOpacityProps {
  type?: 'default' | 'modal';
  tinteColor?: string;
  absolute?: boolean;
  action?(): void;
}

const BackButton: React.FC<ButtonProps> = ({
  type,
  tinteColor,
  absolute = true,
  action,
  ...rest
}) => {
  const navigation = useNavigation();

  return (
    <Container
      {...rest}
      absolute={absolute}
      onPress={() => (!action ? navigation.goBack() : action())}>
      <Icon
        name={!type || type === 'default' ? 'chevron-left' : 'clear'}
        size={30}
        color={tinteColor || '#fff'}
      />
    </Container>
  );
};

export default BackButton;
