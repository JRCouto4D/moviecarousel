import styled, { css } from 'styled-components/native';

interface ButtonProps {
  absolute: boolean;
}

export const Container = styled.TouchableOpacity<ButtonProps>`
  padding: 2% 1.5%;

  align-items: center;
  justify-content: center;

  ${props =>
    props.absolute &&
    css`
      position: absolute;
      top: 5%;
      left: 2%;
    `}
`;
