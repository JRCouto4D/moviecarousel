import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

interface GenresProps {
  data: Array<string>;
}
const Genres: React.FC<GenresProps> = ({ data }) => {
  return (
    <View style={styles.container}>
      {data.map((item, index) => (
        <View key={String(index)} style={styles.content}>
          <Text style={styles.text}>{item}</Text>
        </View>
      ))}
    </View>
  );
};

export default Genres;
