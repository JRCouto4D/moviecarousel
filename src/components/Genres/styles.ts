import { StyleSheet } from 'react-native';

const SPACING = 10;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: SPACING,
  },
  content: {
    borderWidth: 0.5,
    borderColor: '#999',
    borderRadius: SPACING * 2,
    paddingLeft: '5%',
    paddingRight: '5%',
    height: SPACING * 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: SPACING / 2,
  },
  text: {
    color: '#ccc',
    fontSize: 8,
  },
});

export default styles;
