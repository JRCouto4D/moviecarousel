import React from 'react';
import { View } from 'react-native';
import Video from 'react-native-video';
import LinearGradient from 'react-native-linear-gradient';

import video0 from '../../assets/0.mp4';
import video1 from '../../assets/1.mp4';
import video2 from '../../assets/2.mp4';
import video3 from '../../assets/3.mp4';
import video4 from '../../assets/4.mp4';
import video5 from '../../assets/5.mp4';
import styles from './styles';

interface BackgroundProps {
  current: number;
}

const Background: React.FC<BackgroundProps> = ({ current = 0 }) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const data: Record<string, any> = {
    video0,
    video1,
    video2,
    video3,
    video4,
    video5,
  };

  return (
    <View style={styles.container}>
      <Video
        source={data[`video${current}`]}
        repeat
        style={styles.video}
        resizeMode="cover"
      />

      <LinearGradient
        colors={['transparent', '#000']}
        start={{ x: 0.5, y: 0.25 }}
        end={{ x: 0.5, y: 0.4 }}
        style={styles.gradient}
      />
    </View>
  );
};

export default Background;
