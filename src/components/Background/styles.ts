import { StyleSheet } from 'react-native';
import { ww, wh } from '../../utils/dimensions';

const styles = StyleSheet.create({
  container: {
    width: ww(100),
    height: wh(110),
    position: 'absolute',
    top: wh(-10),
  },
  video: {
    width: ww(100),
    height: wh(70),
    position: 'absolute',
    top: 0,
  },
  gradient: {
    width: ww(100),
    height: wh(120),
    position: 'absolute',
  },
});

export default styles;
