import styled, { css } from 'styled-components/native';

import TextDynamic from '../../../TextDynamic';

interface ButtonProps {
  separator: boolean;
}

export const Button = styled.TouchableOpacity<ButtonProps>`
  flex: 1;

  align-items: center;
  justify-content: center;

  padding: 2% 5%;

  ${props =>
    props.separator &&
    css`
      border-right-width: 1px;
      border-color: #ccc;
    `}
`;

export const ButtonText = styled(TextDynamic)``;
