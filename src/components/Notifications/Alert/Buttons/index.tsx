import React from 'react';
import { useDispatch } from 'react-redux';

import { Button as ButtonTypes } from '../../../../store/ducks/notifications/types';
import { hideAlert } from '../../../../store/ducks/notifications/actions';

import { Button, ButtonText } from './styles';

interface ButtonProps {
  button: ButtonTypes;
  separator?: boolean;
}

const Buttons: React.FC<ButtonProps> = ({ button, separator }) => {
  const dispatch = useDispatch();

  return (
    <Button
      separator={separator || false}
      onPress={() => {
        if (button.onPress) {
          button.onPress();
        }
        dispatch(hideAlert());
      }}>
      <ButtonText
        isColor={button.style === 'cancel' ? '#FA8072' : '#333'}
        isSize={1.8}>
        {button.text}
      </ButtonText>
    </Button>
  );
};

export default Buttons;
