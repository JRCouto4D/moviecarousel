import styled from 'styled-components/native';
import {
  responsiveHeight as wh,
  responsiveWidth as ww,
  responsiveFontSize as ftSize,
} from 'react-native-responsive-dimensions';

export const Container = styled.View`
  position: absolute;
  top: 0;
  left: 0;

  width: ${ww(100)};
  height: ${wh(100)};

  align-items: center;
  justify-content: center;
  background: rgba(0, 0, 0, 0.6);
`;
