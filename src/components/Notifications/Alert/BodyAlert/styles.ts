import styled from 'styled-components/native';
import {
  responsiveWidth as ww,
  responsiveFontSize as ftSize,
} from 'react-native-responsive-dimensions';

export const Container = styled.View`
  width: ${ww(80)};
  padding: 5%;

  background: #f5f5f5;
  border-radius: 10px;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Text = styled.Text`
  margin-left: 2%;

  font-size: ${ftSize(2)};
  font-weight: bold;
  color: #333;
`;

export const Title = styled.Text`
  font-size: ${ftSize(2)};
  font-weight: bold;
  color: #333;

  margin-top: 2%;
`;

export const Description = styled.Text`
  font-size: ${ftSize(1.5)};
  color: #999;
  margin-top: -1%;
`;

export const ButtonsContainer = styled.View`
  width: 100%;

  flex-direction: row;
  align-items: center;
  justify-content: flex-end;

  margin-top: 10%;
  padding-top: 2.5%;

  border-top-width: 1px;
  border-color: #ccc;
`;
