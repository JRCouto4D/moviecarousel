import React from 'react';
import { useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/Feather';

import { Button, Alert } from '../../../../store/ducks/notifications/types';
import { hideAlert } from '../../../../store/ducks/notifications/actions';

import Buttons from '../Buttons';

import {
  Container,
  Header,
  Text,
  Title,
  Description,
  ButtonsContainer,
} from './styles';

interface AlertProps {
  alert: Alert | null;
}

type ButtonsTypes = Button[];

const BodyAlert: React.FC<AlertProps> = ({ alert }) => {
  const dispatch = useDispatch();

  const buttons: ButtonsTypes = alert?.buttons || [];

  return (
    <Container>
      <Header>
        <Icon name="alert-circle" size={30} color="#333" />
        <Text>ATENÇÃO</Text>
      </Header>
      <Title>{alert?.title || ''}</Title>
      <Description>{alert?.description || ''}</Description>

      <ButtonsContainer>
        {buttons.length >= 1 ? (
          buttons.map((button, index) => (
            <Buttons
              key={String(index)}
              button={button}
              separator={buttons.length > index + 1}
            />
          ))
        ) : (
          <Buttons
            button={{
              text: 'OK',
              onPress: () => dispatch(hideAlert()),
              style: 'default',
            }}
          />
        )}
      </ButtonsContainer>
    </Container>
  );
};

export default BodyAlert;
