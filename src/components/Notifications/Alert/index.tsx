import React from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';

import { ApplicationState } from '../../../store';

import BodyAlert from './BodyAlert';

import { Container } from './styles';

const Alert: React.FC = () => {
  const { alert } = useSelector(
    (state: ApplicationState) => state.notifications.alerts,
  );

  if (!alert) {
    return <View />;
  }

  return (
    <Container>
      <BodyAlert alert={alert} />
    </Container>
  );
};

export default Alert;
