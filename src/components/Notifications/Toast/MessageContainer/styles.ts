import styled, { css } from 'styled-components/native';
import { responsiveWidth as ww } from 'react-native-responsive-dimensions';
import Animated from 'react-native-reanimated';

import TextDynamic from '../../../TextDynamic';

interface ToastProps {
  type?: 'info' | 'error' | 'success';
}

const toastTypeVariations = {
  info: css`
    background: #f0f0df;
  `,
  success: css`
    background: #e6fffa;
  `,
  error: css`
    background: #fddede;
  `,
};

export const Container = styled(Animated.View).attrs({
  elevation: 2,
  shadowColor: '#000',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.1,
  shadowRadius: 5,
})`
  width: ${ww(60)};
  border-radius: 5px;

  margin-bottom: 3%;
`;

export const Content = styled.View<ToastProps>`
  padding: 5%;
  border-radius: 5px;
  margin: 0 0.5px;

  ${props => toastTypeVariations[props.type || 'info']}
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;

  margin-bottom: 2%;
`;

export const Text = styled(TextDynamic)``;
