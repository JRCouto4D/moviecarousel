import React, { useEffect } from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {
  useSharedValue,
  useAnimatedStyle,
  withTiming,
  interpolate,
  Extrapolate,
  Easing,
} from 'react-native-reanimated';
import { responsiveWidth as ww } from 'react-native-responsive-dimensions';
import { useDispatch } from 'react-redux';

import { removeToast } from '../../../../store/ducks/notifications/actions';

import { Message } from '../../../../store/ducks/notifications/types';

import { Container, Content, Header, Text } from './styles';

interface MessageProps {
  data: Message;
}
const MessageContainer: React.FC<MessageProps> = ({
  data: { id, type, title, description },
}) => {
  const dispatch = useDispatch();

  const textColorVariations = {
    info: '#c1bc35',
    success: '#2e656a',
    error: '#c53030',
  };

  const iconTypeVariations = {
    info: 'info',
    error: 'alert-triangle',
    success: 'check-circle',
  };

  const initialPosition = ww(63);

  const containerPosition = useSharedValue(initialPosition);

  const containerStyle = useAnimatedStyle(() => ({
    transform: [{ translateX: containerPosition.value }],
    opacity: interpolate(
      containerPosition.value,
      [initialPosition, 0],
      [0, 1],
      Extrapolate.CLAMP,
    ),
  }));

  useEffect(() => {
    containerPosition.value = withTiming(0, { duration: 500 });

    setTimeout(() => {
      containerPosition.value = withTiming(initialPosition, {
        duration: 500,
        easing: Easing.elastic(),
      });
    }, 3000);

    setTimeout(() => {
      dispatch(removeToast(id));
    }, 4000);
  });
  return (
    <Container style={containerStyle}>
      <Content type={type}>
        <Header>
          <Icon
            name={iconTypeVariations[type]}
            size={18}
            color={textColorVariations[type]}
          />
          <Text
            isSize={1.4}
            isColor={textColorVariations[type]}
            style={{ marginLeft: '2%', fontWeight: 'bold' }}>
            {title}
          </Text>
        </Header>
        <Text isSize={1.1} isColor={textColorVariations[type]}>
          {description}
        </Text>
      </Content>
    </Container>
  );
};

export default MessageContainer;
