import styled from 'styled-components/native';
import { responsiveWidth as ww } from 'react-native-responsive-dimensions';

export const Container = styled.View`
  width: ${ww(60)};
  position: absolute;
  top: 5%;
  right: ${ww(2)};
  border-radius: 5px;
`;
