import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { ApplicationState } from '../../../store';

import Message from './MessageContainer';

import { Container } from './styles';

const Toast: React.FC = () => {
  const {
    toast: { messages },
  } = useSelector((state: ApplicationState) => state.notifications);

  const renderMessages = useMemo(
    () => messages.map(message => <Message key={message.id} data={message} />),
    [messages],
  );

  return <Container>{renderMessages}</Container>;
};

export default Toast;
