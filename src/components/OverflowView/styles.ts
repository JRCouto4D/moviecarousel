import { StyleSheet } from 'react-native';

const OVERFLOW_HEIGHT = 70;
const SPACING = 10;

const styles = StyleSheet.create({
  overflowContainer: {
    height: OVERFLOW_HEIGHT,
    overflow: 'hidden',
    marginTop: SPACING * 2,
    // backgroundColor: '#312e38',
  },
  overflowContainerRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  overflowItemContainer: {
    height: OVERFLOW_HEIGHT,
    padding: SPACING,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    textTransform: 'uppercase',
    letterSpacing: -1,
    color: '#f5f5f5',
  },
  where: {
    fontSize: 12,
    color: '#c5c5c5',
  },
  date: {
    fontSize: 12,
    color: '#c5c5c5',
  },
});

export default styles;
