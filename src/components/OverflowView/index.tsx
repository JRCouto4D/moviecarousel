import React from 'react';
import { View, Text, Animated } from 'react-native';
import { format, parseISO } from 'date-fns';

import { MoviesType } from '../../services/movies';

import styles from './styles';

interface OverflowProps {
  data: MoviesType[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  scrollXAnimated: any;
}

const OVERFLOW_HEIGHT = 70;

const OverflowView: React.FC<OverflowProps> = ({ data, scrollXAnimated }) => {
  const inputRange = [-1, 0, 1];
  const translateY = scrollXAnimated.interpolate({
    inputRange,
    outputRange: [OVERFLOW_HEIGHT, 0, -OVERFLOW_HEIGHT],
  });

  return (
    <View style={styles.overflowContainer}>
      <View>
        {data.map(item => {
          return (
            <Animated.View
              key={item.key}
              style={[
                {
                  transform: [
                    {
                      translateY,
                    },
                  ],
                },
                styles.overflowItemContainer,
              ]}>
              <Text style={styles.title} numberOfLines={1}>
                {item.title}
              </Text>
              <View style={styles.overflowContainerRow}>
                <Text style={styles.where} numberOfLines={1}>
                  {item.where}
                </Text>

                <Text style={styles.date}>
                  {format(parseISO(item.debut), "dd MMM', 'yyyy")}
                </Text>
              </View>
            </Animated.View>
          );
        })}
      </View>
    </View>
  );
};

export default OverflowView;
