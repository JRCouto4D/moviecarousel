import styled from 'styled-components/native';
import { responsiveFontSize as ftSize } from 'react-native-responsive-dimensions';

interface TextProps {
  isType?: string;
  isSize?: number;
  isColor?: string;
}

export const Text = styled.Text<TextProps>`
  font-size: ${props => ftSize(props.isSize || 1.2)};
  color: ${props => props.isColor || '#333'};
`;
