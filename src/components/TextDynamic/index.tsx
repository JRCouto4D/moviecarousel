import React from 'react';
import { TextProps } from 'react-native';

import { Text } from './styles';

interface TextDynamicProps extends TextProps {
  isType?: string;
  isSize?: number;
  isColor?: string;
}

const TextDynamic: React.FC<TextDynamicProps> = ({
  isType,
  isSize,
  isColor,
  style,
  children,
}) => (
  <Text isType={isType} isColor={isColor} isSize={isSize} style={style}>
    {children}
  </Text>
);

export default TextDynamic;
